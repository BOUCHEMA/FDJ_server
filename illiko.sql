-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 26, 2017 at 12:39 AM
-- Server version: 5.7.17-0ubuntu0.16.04.1
-- PHP Version: 7.0.15-0ubuntu0.16.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `illiko`
--

--
-- Dumping data for table `illiko`
--

INSERT INTO `illiko` (`id`, `title`, `img`, `href`, `status`) VALUES
(1, 'Astro', 'astro.jpg', 'https://www.fdj.fr/jeux/illiko-jeux-de-grattage', 0),
(2, 'Banco', 'banco.jpg', 'https://www.fdj.fr/jeux/illiko-jeux-de-grattage', 1),
(3, 'Black Jack', 'black-jack.jpg', 'https://www.fdj.fr/jeux/illiko-jeux-de-grattage', 0),
(4, 'Cash', 'cash.jpg', 'https://www.fdj.fr/jeux/illiko-jeux-de-grattage', 1),
(5, 'Eldorado', 'eldorado.jpg', ' https://www.fdj.fr/jeux/illiko-jeux-de-grattage', 0),
(6, 'La ruche d or', 'la-ruche-d-or.jpg', 'https://www.fdj.fr/jeux/illiko-jeux-de-grattage', 0),
(7, 'Millionnaire', 'millionnaire.jpg', 'https://www.fdj.fr/jeux/illiko-jeux-de-grattage', 1),
(8, 'Solitaire', 'solitaire.jpg', 'https://www.fdj.fr/jeux/illiko-jeux-de-grattage', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
