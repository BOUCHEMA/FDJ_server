<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Illiko
 *
 * @ORM\Table(name="illiko")
 * @ORM\Entity
 */
class Illiko
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=4000, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="img", type="string", length=4000, nullable=true)
     */
    private $img;

    /**
     * @var string
     *
     * @ORM\Column(name="href", type="string", length=4000, nullable=true)
     */
    private $href;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;


}

