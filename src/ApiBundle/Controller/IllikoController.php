<?php

namespace ApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use ApiBundle\Controller\ApiController;

/**
 * @RouteResource("illiko")
 */
class IllikoController extends ApiController
{
    /**
     * @Get("/illiko", name="get")
     * @ApiDoc(
     *  section="Illiko",
     *  description="Renvoie la liste des jeux , et filtrée par statut si il est indiqué",
     *  output="ApiBundle\Entity\Illiko"
     * )
     * @param int $status
     * @View
     * @return array
     */
    public function getAction()
    {
        return $this->call('getList');
    }
    /**
     * @Get("/illiko/status/{status}", requirements= { "status": "[01]"}, name="get_by_status")
     * @ApiDoc(
     *  section="Illiko",
     *  description="Renvoie la liste des jeux, et filtrée par statut si il est indiqué",
     *  output="ApiBundle\Entity\Illiko"
     * )
     * @param int $status
     * @View
     * @return array
     */
    public function getByStatusAction($status = null)
    {
        return $this->call('getListBy', [['status' => $status]]);
    }
    /**
     * @Post("/illiko", name="add_game")
     * @ApiDoc(
     *  section="Illiko",
     *  description="Ajoute un jeu",
     *  input="ApiBundle\Form\Type\IllikoType",
     *  output="ApiBundle\Entity\Illiko"
     * )
     * @param int $status
     * @View
     * @return array
     */
    public function addAction(Request $request)
    {
        return $this->call('post', [$request]);
    }
    /**
     * @Put("/illiko", name="modify_game")
     * @ApiDoc(
     *  section="Illiko",
     *  description="Modifie un jeu",
     * filters={
     *     {"name"="name", "dataType"="string"},
     * },
     *  output="ApiBundle\Entity\Illiko"
     * )
     * @param int $status
     * @View
     * @return array
     */
    public function modifyAction(Request $request)
    {
        return $this->call('put', [$request]);
    }

    /**
     * @Delete("/illiko/{id}", requirements= { "id": "\d+"}, name="delete_game")
     * @ApiDoc(
     *  section="Illiko",
     *  description="Supprime un jeu",
     * )
     * @param int $status
     * @View
     * @return array
     */
    public function deleteAction(Request $request)
    {
        return $this->call('remove', [$request]);
    }

    /**
     * @Patch("/illiko/{id}", name="open_game")
     * @ApiDoc(
     *  section="Illiko",
     *  description="passe le statut du jeu selectionné à 1",
     *  input="ApiBundle\Form\Type\IllikoType",
     *  output="ApiBundle\Entity\Illiko"
     * )
     * @param int $status
     * @View
     * @return array
     */
    public function openGameAction(Request $request)
    {
        $request->request->set('illiko', ['status' => 1]);
        return $this->call('modify', [$request, false]);
    }
    /**
     * @Patch("/illiko/{id}", name="close_game")
     * @ApiDoc(
     *  section="Illiko",
     *  description="passe le statut du jeu selectionné à 0",
     *  input="ApiBundle\Form\Type\IllikoType",
     *  output="ApiBundle\Entity\Illiko"
     * )
     * @param int $status
     * @View
     * @return array
     */
    public function closeGameAction(Request $request)
    {
        $request->request->set('illiko', ['status' => 0]);
        return $this->call('modify', [$request, false]);
    }
}
