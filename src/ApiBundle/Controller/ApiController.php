<?php

namespace ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

abstract class ApiController extends FOSRestController
{
    private $em;

    private $repo;

    private $type;
    private $entity;
    private $param;

    public function getSecureResourceAction()
    {
        if (false === $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw new AccessDeniedException();
        }
    }

    protected function getList()
    {
        return $this->repo->findAll();
    }

    protected function getListBy($params)
    {
        return $this->repo->findBy($params);
    }

    protected function post(Request $request)
    {
        $class = $this->entity;
        $entity = new $class();
        $form = $this->createForm($this->type, $entity);
        $form->submit($request->request->get($this->param)); // Validation des données
        if ($form->isValid()) {
            $this->em->persist($entity);
            $this->em->flush();
            return $entity;
        } else {
            return $form;
        }
    }

    protected function remove(Request $request)
    {
        $entity = $this->repo->find($request->get('id'));
        $this->em->remove($entity);
        $this->em->flush();
    }

    protected function modify(Request $request, $clearMissing)
    {
        $entity = $this->repo->find($request->get('id')); 
        if (empty($entity)) {
            return new JsonResponse(['message' => ucfirst($this->param).' not found'], 400);
        }

        $form = $this->createForm($this->type, $entity);
        $form->submit($request->request->get($this->param), $clearMissing);

        if ($form->isValid()) {
            $this->em->merge($entity);
            $this->em->flush();

            return $entity;
        } else {
            return $form;
        }
    }

    protected function call($method, $args = [])
    {   
        try {
            $view = $this->view(call_user_func_array([$this, $method], $args));    
        } catch (\Exception $e) {
            $this->get('logger')->error($e->getMessage());
            $view = $this->view(['error' => $e->getMessage()], 400);          
        }

        return $this->handleView($view);
    }
}
