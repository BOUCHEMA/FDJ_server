<?php


namespace ApiBundle\Command\Composer;

use Sensio\Bundle\DistributionBundle\Composer\ScriptHandler as SensioScriptHandler;
use Composer\Script\Event;

class ScriptHandler extends SensioScriptHandler
{
    /**
     * Clears the Symfony cache.
     *
     * @param Event $event
     */
    public static function initDatabase(Event $event)
    {
        $options = static::getOptions($event);
        $consoleDir = static::getConsoleDir($event, 'init the database(client and user)');

        if (null === $consoleDir) {
            return;
        }

        static::executeCommand($event, $consoleDir, 'api:init:database ', $options['process-timeout']);
    }
}
