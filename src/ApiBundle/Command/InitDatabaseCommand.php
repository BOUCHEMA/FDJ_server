<?php

namespace ApiBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use ApiBundle\Entity\User;

class InitDatabaseCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('api:init:database')
            ->setDescription('Init database')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $clientManager = $this->getContainer()->get('fos_oauth_server.client_manager.default');
        $client = $clientManager->createClient();
        $client->setRandomId('q8s6uuv3mcgkkcg880k4ow40ok4s00g8888w4ogcs84ogos84');
        $client->setSecret('4lc61bohhmkgc844os44o4cco08gk4w4ssk8o0ckcwk8c0kow8');
        $client->setAllowedGrantTypes(['password']);
        try {
            $em->persist($client);
            $em->flush();
        } catch (\Exception $e) {
            dump($e->getMessage());
            
        }
        $output->writeln(sprintf('Added a new client with public id <info>%s</info>.', $client->getPublicId()));        
        $user = new User();
        $user->setUsername('test');
        $user->setEmail('test@test.com');
        $user->setEnabled(1);
        $user->setPlainPassword('test');
        try {
            $em->persist($user);
            $em->flush();
        } catch (\Exception $e) {

        }
        $output->writeln('Added a new user with name test and password test.');
    }
}
