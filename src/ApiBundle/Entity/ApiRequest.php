<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="api_request")
 */
class ApiRequest
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="route", type="string", length=100, nullable=false)
     */
    private $route = '';

    /**
     * @var string
     *
     * @ORM\Column(name="args", type="string", length=4000, nullable=true)
     */
    private $args;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="float", nullable=true)
     */
    private $elapsedTime;

    public function __construct()
    {
    }

    /**
     * Gets the value of id.
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Gets the value of route.
     *
     * @return string
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Sets the value of route.
     *
     * @param string $route the route
     *
     * @return self
     */
    public function setRoute($route)
    {
        $this->route = $route;

        return $this;
    }

    /**
     * Gets the value of args.
     *
     * @return string
     */
    public function getArgs()
    {
        return $this->args;
    }

    /**
     * Sets the value of args.
     *
     * @param string $args the args
     *
     * @return self
     */
    public function setArgs($args)
    {
        $this->args = $args;

        return $this;
    }

    /**
     * Gets the value of elapsedTime.
     *
     * @return string
     */
    public function getElapsedTime()
    {
        return $this->elapsedTime;
    }

    /**
     * Sets the value of elapsedTime.
     *
     * @param string $elapsedTime the elapsed time
     *
     * @return self
     */
    public function setElapsedTime($elapsedTime)
    {
        $this->elapsedTime = $elapsedTime;

        return $this;
    }
}
