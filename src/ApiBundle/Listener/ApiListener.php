<?php

namespace ApiBundle\Listener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use ApiBundle\Services\SecurityControl;
use ApiBundle\Entity\ApiRequest;
/**
 * ApiListener
 */
class ApiListener
{
    
    private $apiClass;

    private $securityControl;

    private $logger;

    /**
     * ApiListener constructor.
     *
     * @param string           $apiClass
     * @param SecurityControl $securityControl
     */
    public function __construct($apiClass, $securityControl, $logger)
    {
        $this->apiClass = $apiClass;
        $this->securityControl = $securityControl;
        $this->logger = $logger;
    }

    /**
     *
     * @param FilterControllerEvent $event
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();
        $controller = $controller[0];

        if (!is_array($controller) && get_parent_class($controller) == $this->apiClass) {

            $this->securityControl->setApiRequest(new ApiRequest());
            try {
                $controller->getSecureResourceAction();
            } catch (\Exception $exception) {
                $this->securityControl->saveRequest();
                $message = $exception->getMessage();
                $event->setController(function () use ($message) {
                    $this->logger->error($message);
                    return new JsonResponse($message, 400);
                });
            }
            $this->securityControl->initialize($controller);
        }
    }

    /**
     *
     * @param FilterResponseEvent $event
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        if (strpos($event->getRequest()->get('_route'), 'api_') === 0 && $event->getResponse()->headers->get('Content-Type') == "application/json") {
            if ($this->securityControl->getApiRequest()) {
                $this->securityControl->succesRequest();
                $this->securityControl->setApiRequest(null);
            }
        }
    }
}
