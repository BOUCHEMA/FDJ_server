<?php

namespace ApiBundle\Services;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Monolog\Logger;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class SecurityControl
 * @package ApiBundle\Services
 */
class SecurityControl
{
    /**
     * @var Registry
     */
    private $em;

    /**
     * @var RequestStack
     */
    private $request;

    /**
     * @var ApiRequest
     */
    private $apiRequest;

    private $start;
    /**
     * @var Logger
     */
    private $logger;

    /**
     * SecurityControl constructor.
     * @param Registry             $doctrine
     * @param RequestStack         $requestStack
     * @param Logger               $logger
     */
    public function __construct($em, RequestStack $requestStack, Logger $logger)
    {
        $this->request = $requestStack->getMasterRequest();
        $this->logger = $logger;
        $this->em = $em;
        $this->start = microtime(true);
    }

    public function initialize($controller)
    {
        $controller->em = $this->get('doctrine.orm.entity_manager');
        $classe = str_replace(['\\', 'Controller', '::'], [':', '', ':'], get_class($this));
        $controller->repo = $controller->em->getRepository($classe);
        $controller->param = explode(':', strtolower($classe))[1];
        $controller->entity = str_replace(['\\Controller\\', 'Controller'], ['\\Entity\\', ''], get_class($controller));
        $controller->type = str_replace(['\\Controller\\', 'Controller'], ['\\Form\\Type\\', 'Type'], get_class($controller));
    }

    /**
     * @return ApiRequest
     */
    public function getApiRequest()
    {
        return $this->apiRequest;
    }

    /**
     * @param ApiRequest $apiRequest
     * @return SecurityControl
     */
    public function setApiRequest($apiRequest)
    {
        $this->apiRequest = $apiRequest;

        return $this;
    }

    /**
     *
     * @throws \Exception
     * @return JsonResponse
     */
    public function checkHeader()
    {
        return true;
    }

    /**
     */
    public function successRequest()
    {
        preg_match("/([\w\-]+Controller)::(\w+Action)/", $this->request->attributes->get('_controller'), $searchResult);
        $type = strtolower(str_replace('Controller', '', $searchResult[1]));
        $this->apiRequest->setElapsedTime( microtime(true) - $this->start);
        $this->logger->addInfo('Request succesful', ['reference' => 'api', 'type' => $type, 'elapsed' => $this->apiRequest->getElapsedTime()]);
 
        $this->saveRequest();
    }

    /**
     *
     * @param string $message
     * @param int    $code
     * @return array
     */
    public function failureRequest($message, $code)
    {
        preg_match("/([\w\-]+Controller)::(\w+Action)/", $this->request->attributes->get('_controller'), $searchResult);
        $type = strtolower(str_replace('Controller', '', $searchResult[1]));
        $this->apiRequest->setElapsedTime( microtime(true) - $this->start);
        $this->logger->addError($message, ['reference' => 'api', 'type' => $type, 'elapsed' => $this->apiRequest->getElapsedTime()]);
        $this->saveRequest();

        return json_encode(array(array("error" => $message, "error_description" => $this->error[$message]), $code));
    }

    /**
     * Sauvegarde des données de la requête en BDD
     */
    public function saveRequest()
    {
        $this->em->persist($this->apiRequest);
        $this->em->flush();
    }
}
