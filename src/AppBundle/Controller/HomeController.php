<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;

class HomeController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Method("GET")
     */
    public function indexAction()
    {
        $data = $this->get('api_connection')->authenticate();
        $openedGames = $this->get('api_connection')->getOpenedGames($data->access_token);
        return $this->render(
            'AppBundle:Home:index.html.twig',
            [
                'carousel' => [
                    'data' => $openedGames,
                    'id'   => 'carousel_illiko',
                ]
            ]
        );
    }

    /**
     * @Route("/token", name="get_token")
     * @Method("GET")
     */
    public function getTokenAction()
    {
        return new JsonResponse($this->get('api_connection')->authenticate());
    }    
}
