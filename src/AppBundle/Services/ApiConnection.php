<?php

namespace AppBundle\Services;

class ApiConnection
{

    private $client;

    private $config;

    public function __construct($config)
    {
        $this->config = $config;
        $this->client = new \GuzzleHttp\Client();
    }
     
    public function authenticate()
    {
        // Create a POST request
        $response = $this->client->request(
            'POST',
            $this->config['baseUrl'].$this->config['route']['authentication'],
            [
                'form_params' => [
                    'grant_type'    => 'password',
                    'client_id'     => '1_q8s6uuv3mcgkkcg880k4ow40ok4s00g8888w4ogcs84ogos84',
                    'client_secret' => '4lc61bohhmkgc844os44o4cco08gk4w4ssk8o0ckcwk8c0kow8',
                    'username'      => 'test',
                    'password'      => 'test'
                ]
            ]
        );

        return json_decode($response->getBody()->getContents());
    }

    public function getOpenedGames($accessToken)
    {
        $response = $this->client->request(
            'GET',
            $this->config['baseUrl'].$this->config['route']['getIllikoByStatusOpen'], 
            [
                'headers' => [
                    'Authorization'    => 'Bearer '.$accessToken,
                ]
            ]
        );

        return json_decode($response->getBody()->getContents());
    }
}
